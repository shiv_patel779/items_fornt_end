import React from 'react';
import { Link } from 'react-router'

class Header extends React.Component {
  render() {
    const currentPath = this.props.currentPath;
    const tasks = currentPath == '/tasks' ? 'active' : ''
    const calendar = currentPath == '/calendar' ? 'active' : ''
    const activeSignin = currentPath == '/signin' ? 'active' : ''
    const activeSignup = currentPath == '/signup' ? 'active' : ''
    const activeSignout = currentPath == '/signup' ? 'active' : ''
    return (
      <div className='Header'>
        <nav className='navbar navbar-default'>
          <div className='container-fluid'>
            <div className='navbar-header'>
              <button type='button' className='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>
                <span className='sr-only'>Toggle navigation</span>
                <span className='icon-bar'></span>
                <span className='icon-bar'></span>
                <span className='icon-bar'></span>
              </button>
              <Link to='/' className='navbar-brand'>Test App</Link>
            </div>

            <div className='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
              <ul className='nav navbar-nav'>
                <li className={tasks}><Link to='/tasks'>Tasks</Link></li>
                <li className=""><Link to='calendar'>Calendar</Link></li>
                <li className=""><Link to='pending'>Pending Task</Link></li>                
                <li className=""><Link to='completed'>Completed Task</Link></li>
              </ul>            
              { this.props.loggedIn ? (
                <ul className='nav navbar-nav navbar-right'>
                  <li className={activeSignout}><Link to='signout'>Sign Out</Link></li>
                </ul>
              ) : ( 
                <ul className='nav navbar-nav navbar-right'>
                  <li className={activeSignin}><Link to='signin'>Sign in</Link></li>
                  <li className={activeSignup}><Link to='signup'>Sign up</Link></li>
                </ul>
              ) }
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

export default Header;
