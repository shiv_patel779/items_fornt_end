import React from 'react';
import Tasks from './Tasks';
import auth from '../auth'
import flash from '../flash'

class Body extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tasks: []
    };
  };

  componentDidMount() { 
    var token = this.props.state.token;
    $.ajax({
      url: 'http://localhost:3001/items/pending',
      type: 'GET',
      headers: { 'Authorization': `Token ${token}`},
      success:(response) => {    
        this.setState({ tasks: response })
      },
      error:(response) => {
        flash.error(response.responseText)
      }
    });
  }

  render() {
    var state = this.props.state;
    
    return (
      <div className='Body'>       
       <Tasks tasks={this.state.tasks} state={this.props.state} />
      </div>
    )
  }
}

export default Body;
