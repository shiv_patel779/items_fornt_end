import React from 'react';
import auth from '../auth'
import flash from '../flash'

class Note extends React.Component {
  
  constructor(props) {
    super(props);    
  };

  noteSubmit(e) {

    e.preventDefault();
    var token = this.props.state.token;
    var id = this.props.stateNote.id;
    var note = $(this.refs.note).val();
     $.ajax({
        url: 'http://localhost:3001/items/'+id,
        type: 'PUT',
        data: { item: { note: note }},
        headers: { 'Authorization': `Token ${token}` },
        context: this,
        async: false,
        success:(response) => {          
          $('#myModal').modal('toggle');
          this.props.handleNoteSubmit(response);
        }
      });
  }

  render() {

    return (      
      <div className="container">
        <form>       
          <div className="modal fade" id="myModal" role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal">&times;</button>
                  <h4 className="modal-title">Modal Header</h4>
                </div>
                <div className="modal-body">                
                  <textarea
                    id= {"note_"+this.props.stateNote.id}
                    name="note"
                    ref= "note"
                    defaultValue={this.props.stateNote.note.toString()}
                    placeholder="Notes.."/>
                </div>
                <div className="modal-footer">
                  <button type="button" class="btn btn-default" onClick={this.noteSubmit.bind(this)}>Submit</button>                  
                </div>
              </div>
            </div>
          </div>
         
        </form>  
      </div>
   )
  }
}

export default Note;
