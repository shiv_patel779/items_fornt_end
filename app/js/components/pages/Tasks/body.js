import React from 'react';
import Tasks from './Tasks';
import Form from './form';
import auth from '../auth'
import flash from '../flash'
import Note from './Note';

class Body extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      note: '',
      id: ''
    };
  };

  componentDidMount() { 
    var token = this.props.state.token;
    $.ajax({
      url: 'http://localhost:3001/items',
      type: 'GET',
      headers: { 'Authorization': `Token ${token}`},
      success:(response) => {        
        this.setState({ tasks: response })
      },
      error:(response) => {
        flash.error(response.responseText)
      }
    });
  }

  addTask(task) {
    var newState = [task].concat(this.state.tasks);
    this.setState({ tasks: newState });
  }

  handleState(task){
    var tasks = this.state.tasks;
    for (var i in tasks) {
      if (tasks[i].id == task.id) {
        tasks[i].spent_time = task.spent_time;
      }
    }
    this.setState({tasks: tasks});
  }

  handleNoteSubmit(task){
    var tasks = this.state.tasks;
    for (var i in tasks) {
      if (tasks[i].id == task.id) {
        tasks[i].note = task.note;
      }
    }
    this.setState({tasks: tasks});
  }

  setNote(task){

    $("#note_"+task.id).val(task.note);
    this.setState({note: task.note, id: task.id});   
    $('#myModal').modal('toggle');

    $("#myModal textarea").val(task.note)

  }

  render() {
    var state = this.props.state;
    
    return (
      <div className='Body'>
        { ( state.loggedIn ) ?
          ( <div><Form state={this.props.state} addTask={this.addTask.bind(this)}/><br/></div> ) :
          ( <div></div> )
        }
        <Tasks tasks={this.state.tasks} state={this.props.state}  handleState={this.handleState.bind(this)} setNote={this.setNote.bind(this)}/>
        <Note handleNoteSubmit={this.handleNoteSubmit.bind(this)} state={this.props.state} stateNote={this.state} />
      </div>
    )
  }
}

export default Body;
