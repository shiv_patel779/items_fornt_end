import React from 'react';
import auth from '../auth'
import flash from '../flash';

class Task extends React.Component {
  
  constructor(props) {
    super(props); 

    this.state = {
      pending: this.props.task.pending, 
      completed: this.props.task.completed,
      spent_time: this.props.task.spent_time
    };
    
  };

  handlePendingStatus(id) {    
      var token = this.props.state.token;
      $.ajax({
         url: 'http://localhost:3001/items/'+id,
        type: 'PUT',
        data: { item: {pending: !this.state.pending}},
        headers: { 'Authorization': `Token ${token}` },
        context: this,
        async: false,
        success:(response) => {
          this.setState({ pending: response.pending, 
      completed: response.completed  });
        },
        error:(response) => {
          flash.error(response.responseText);
        }
      });      
    
  }

  handleCompleteStatus(id) {    
      var token = this.props.state.token;
      $.ajax({
         url: 'http://localhost:3001/items/'+id,
        type: 'PUT',
        data: { item: { completed: !this.state.completed }},
        headers: { 'Authorization': `Token ${token}` },
        context: this,
        async: false,
        success:(response) => {
           this.setState({ pending: response.pending, 
            completed: response.completed  });
        },
        error:(response) => {
          flash.error(response.responseText);
        }
      });
    
  }
  
  setTimer(id){     
    var getClass = $("#timer_"+id).attr("class").split(" ")
    if( $.inArray('hidden',getClass) == 1) {
      $("#timer_"+id).removeClass('hidden');
      $("#input_"+id).addClass('hidden');
    }else{
      $("#timer_"+id).addClass('hidden');
      $("#input_"+id).removeClass('hidden');
    }
  }

  setTime(id){
    $(".flash-message .alert").removeClass("in");
    var time = $("#timer_"+id).val();
    if ($.isNumeric( time )){
      $(".loader_"+id).removeClass('hidden');
      var token = this.props.state.token;
      $.ajax({
        url: 'http://localhost:3001/items/'+id,
        type: 'PUT',
        data: { item: {spent_time: time}},
        headers: { 'Authorization': `Token ${token}` },
        context: this,
        async: false,
        success:(response) => {
          $(".loader_"+id).addClass('hidden');
          $("#timer_"+id).addClass('hidden');
          $("#input_"+id).removeClass('hidden');
          this.props.handleState(response)       
        }
      });
    }
    else{
      flash.error("Time should be integer.")
      focusElement = $(".flash-message");
      $(focusElement).focus();
      $(window).scrollTop($('.flash-message').position().top);
    }
  }

  setNote(task){
    this.props.setNote(task)
  }
  render() {
    var task = this.props.task
    var state = this.props.state;
    return (
      <tr key={task.id} className='Task'>
        <td width="60%">
          pending
          <input type="checkbox" name="status"
          value={this.state.pending}
          onChange={this.handlePendingStatus.bind(this, task.id)} checked={this.state.pending} />
          Completed
          <input type="checkbox" name="status"
          value={this.state.completed}
          onChange={this.handleCompleteStatus.bind(this, task.id)} checked={this.state.completed} />
        </td>
        <td width="10%">{task.name}</td>
        <td width="15%">
          <a href="#-" onClick={this.setTimer.bind(this,task.id)}>
            <span className="glyphicon glyphicon-time"></span>
          </a>        
          <input type="text" 
            name="time_spent" 
            className="set-width hidden" 
            id={"timer_"+task.id}
            defaultValue= {task.spent_time}
            placeholder="0 hrs" onBlur={this.setTime.bind(this, task.id)}/>
          <label id={"input_"+task.id}>{task.spent_time} </label>
        </td>
        <td width="15%">
          {task.note}
          <a href="#-" onClick={this.setNote.bind(this,task)} data-toggle="modal">
            <span className="glyphicon glyphicon-edit"></span>
          </a>           
        </td>
      </tr>
    )
  }
}

export default Task;
