import React from 'react';
import auth from '../auth'
import flash from '../flash'

class Form extends React.Component {

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  };
  componentDidUpdate() {
   $('#tag_list').tagsInput();
   $('#completion_date').datepicker();
  }
  addTask(task) {
    this.props.addTask(task);
  }
  handleSubmit(e) {
    var token = this.props.state.token;
    e.preventDefault();
    var name = this.refs.name.value;
    var tag_list = $("#tag_list").val();
    var date = this.refs.completion_date.value;
    $.ajax({
      url: "http://localhost:3001/items",
      type: 'POST',
      data: { item: { name: name, tag_list: tag_list, completion_date: date }},
      headers: { 'Authorization': `Token ${token}`},
      context: this,
      success: (response) => {    
        this.refs.name.value = ''
        this.refs.completion_date.value = ''
        $('.tagsinput .tag').remove();
        this.addTask(response)
      },
      error:(response) => {
        if (response.status == 422) {
          flash.errors_with_object(response.responseJSON)
        } else {
          flash.error(response.responseText)
        }
      }
   });
  }
  
  render() {

    return (
      <div className='Form row'>
        <form onSubmit={this.handleSubmit}>
          <div className='col-md-12 name flash-error'>
            <input id="task" ref='name' type='text' name='name' placeholder='Name' className='form-control' required/>
            <br />
            <input id="tag_list" ref='tag_list' type='text' name='tag_list' placeholder='Name' className='form-control'/>
            <br />
            <input id="completion_date" ref='completion_date' type='text' name='completion_date' placeholder='23/9/2016' className='form-control' required  autoComplete="off"/>
            <br />
            <p className='error'></p>
          </div>
          <br/>
          <div className='col-md-2'>
            <input type='submit' value='Submit' className='btn btn-primary' />
          </div>
        </form>  
      </div>
    )
  }
}

export default Form;
