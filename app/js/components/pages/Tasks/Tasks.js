import React from 'react';
import Task from './Task';
var fileDownload = require('react-file-download');

class Tasks extends React.Component {
  
  handleState(response) {
    this.props.handleState(response)
  }

  generateReport(e){    
    var token = this.props.state.token;
    e.preventDefault();    
    $.ajax({
      url: "http://localhost:3001/items/generate_report",
      type: 'get',
      dataType: 'json',
      headers: { 'Authorization': `Token ${token}`},
      context: this,
      success: (response) => {            
        fileDownload(response.data, 'filename.csv');
      }
    });  
    
  }
  render() {
    var tasks = this.props.tasks.map((task) => {
      return (
        <Task task={task} state={this.props.state} handleState={this.handleState.bind(this)} setNote={this.props.setNote}/>
      ); 
    });
    var state = this.props.state;
    
    return (
      <div className='Tasks'>
        <input type='submit' value='Generate Report' className='btn btn-primary right' onClick={this.generateReport.bind(this)}/>
        <table className='table'>
          <tbody>
            <tr>
              <th width="60%">Actions</th>
              <th width="10%">Name</th>
              <th width="15%">Timer</th>
              <th width="15%">Note</th>
            </tr>            
            {tasks}
          </tbody>
        </table>
      </div>
    )
  }
}

export default Tasks;
