import React from 'react';

class Welcome extends React.Component {  
  render() {
    return (
      <div>        
        Welcome to my site!
      </div>
    )
  }
}

export default Welcome;
