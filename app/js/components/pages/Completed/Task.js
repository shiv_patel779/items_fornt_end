import React from 'react';
import auth from '../auth'
import flash from '../flash'

class Task extends React.Component {  
  render() {
    var task = this.props.task
    var state = this.props.state;
    return (
      <tr key={task.id} className='Task'>          
        <td>{task.name}</td>
      </tr>
    )
  }
}

export default Task;
