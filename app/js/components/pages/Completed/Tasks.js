import React from 'react';
import Task from './Task';

class Tasks extends React.Component {
  
  handleDelete(id) {
    this.props.handleDelete(id)
  }

  render() {
    var tasks = this.props.tasks.map((task) => {
      return ( 
        <Task task={task} state={this.props.state} />
      ); 
    });
    var state = this.props.state;
    
    return (
      <div className='Tasks'>
        <table className='table'>
          <tbody>
            <tr>              
              <th>Name</th>
            </tr>
            {tasks}
          </tbody>
        </table>
      </div>
    )
  }
}

export default Tasks;
