import React from 'react';
import Header from './header';
import Body from './body';

class Main extends React.Component {  
  componentDidMount() { 
    this.loadCalendar();   
  }

  loadCalendar(){
    var token = this.props.state.token;
    $.ajax({
      url: 'http://localhost:3001/items',
      type: 'GET',
      headers: { 'Authorization': `Token ${token}`},
      success:(response) => {    
        var tasks = [];
        var i, task;        
        for (i=0; i<(response.length); i++) {
          if (response[i].completion_date != null){
            task = { "title" : response[i].name,
                   "start" : response[i].completion_date,
                   "end"   : response[i].completion_date,
                 }
          }       
          tasks.push(task);
        }

        $('.calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
          },      
          navLinks: true, // can click day/week names to navigate views
          editable: true,
          eventLimit: true, // allow "more" link when too many events
          events:tasks
        });
      }
    });
  }
  render() {
    return (
      <div className='calendar'>      	
      </div>
    )
  }
}

export default Main;
