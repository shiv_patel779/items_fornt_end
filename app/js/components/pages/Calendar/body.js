import React from 'react';
import auth from '../auth'
import flash from '../flash'
import Tasks from '../Tasks/Tasks';

class Body extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      tasks: []
    };
  };

  componentDidMount() { 
    this.getTask('day')
  }
  getTask(sort_by) {
    // $.ajax({
    //   url: `${auth.host()}/space_types`,
    //   type: 'GET',
    //   success:(response) => {
    //     this.setState({ spaceTypes: response.spaceTypes.reverse() })
    //   },
    //   error:(response) => {
    //     flash.error(response.responseText)
    //   }
    // });
  }

  render() {
    var state = this.props.state;
    
    return (
      <div className='Body'>
       <Tasks tasks={this.state.tasks} state={this.props.state} />
      </div>
    )
  }
}

export default Body;
