import React from 'react';
import ReactDOM from 'react-dom';
import { render } from 'react-dom'
import { Router, Route, Link, browserHistory, IndexRoute, withRouter  } from 'react-router'

// Import components
import App from './components/pages/App';
import Tasks from './components/pages/Tasks/main';
import Calendar from './components/pages/Calendar/main';
import Pending from './components/pages/Pending/main';
import Completed from './components/pages/Completed/main';
import Signin from './components/pages/Signin';
import Signup from './components/pages/Signup';
import Signout from './components/pages/Signout';
import NotFound from './components/pages/NotFound';
import Welcome from './components/pages/Welcome';
import auth from './components/pages/auth'

// Creating routes
var routes = (
  <Router history = {browserHistory}>
    <Route path = '/' component = {App}>
      <IndexRoute component = {Welcome} onEnter={requireAuth}/>
      <Route path = 'tasks' name='tasks' component = {Tasks}  onEnter={requireAuth}/>
      <Route path = 'calendar' name='calendar' component = {Calendar}  onEnter={requireAuth}/>
      <Route path = 'pending' name='pending' component = {Pending}  onEnter={requireAuth}/>
      <Route path = 'completed' name='completed' component = {Completed}  onEnter={requireAuth}/>
      <Route path = 'signin' name='signin' component = {Signin}  onEnter={checkAuth}/>
      <Route path = 'signup' name='signup' component = {Signup}  onEnter={checkAuth}/>
      <Route path = 'signout' name='signout' component = {Signout} onEnter={requireAuth} />
      <Route component={NotFound} path="*"></Route>
    </Route>
  </Router>
)

function requireAuth(nextState, replace) {
  if (!auth.loggedIn()) {
    replace({
      pathname: '/signin',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}

function checkAuth(nextState, replace) {
  if (auth.loggedIn()) {
    replace({
      pathname: '/',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}
ReactDOM.render(routes, document.getElementById('app'))

