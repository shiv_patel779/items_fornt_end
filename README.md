# Test app front end

## How do I set this up?

Clone the repository in your local machine:

```
git clone git@bitbucket.org:vivekyuasoft115/test_front_end.git
cd test_front_end
```

Run `npm install` to install all the node_modules we are using.

Run `npm start` to run the app.